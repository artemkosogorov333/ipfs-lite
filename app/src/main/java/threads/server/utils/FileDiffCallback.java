package threads.server.utils;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DiffUtil;

import java.util.List;
import java.util.Objects;

import threads.server.core.files.Proxy;

@SuppressWarnings("WeakerAccess")
public class FileDiffCallback extends DiffUtil.Callback {
    private final List<Proxy> mOldList;
    private final List<Proxy> mNewList;

    public FileDiffCallback(List<Proxy> messages, List<Proxy> proxies) {
        this.mOldList = messages;
        this.mNewList = proxies;
    }

    @Override
    public int getOldListSize() {
        return mOldList.size();
    }

    @Override
    public int getNewListSize() {
        return mNewList.size();
    }

    @Override
    public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
        return mOldList.get(oldItemPosition).areItemsTheSame(mNewList.get(
                newItemPosition));
    }

    @Override
    public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
        return sameContent(mOldList.get(oldItemPosition), mNewList.get(newItemPosition));
    }


    private boolean sameContent(@NonNull Proxy t, @NonNull Proxy o) {

        if (t == o) return true;
        return t.isLeaching() == o.isLeaching() &&
                t.isSeeding() == o.isSeeding() &&
                t.isDeleting() == o.isDeleting() &&
                Objects.equals(t.getSize(), o.getSize()) &&
                Objects.equals(t.getParent(), o.getParent()) &&
                Objects.equals(t.getName(), o.getName()) &&
                Objects.equals(t.getMimeType(), o.getMimeType()) &&
                Objects.equals(t.getCid(), o.getCid()) &&
                Objects.equals(t.getUri(), o.getUri()) &&
                Objects.equals(t.getLastModified(), o.getLastModified());
    }

}
