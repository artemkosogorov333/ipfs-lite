package threads.lite.cid;

import androidx.annotation.NonNull;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Objects;


public final class Prefix {

    private final long version;
    private final long codec;
    private final int mhType;
    private final long mhLength;

    public Prefix(long codec, long mhLength, int mhType, long version) {
        this.version = version;
        this.codec = codec;
        this.mhType = mhType;
        this.mhLength = mhLength;
    }

    public static Prefix getPrefixFromBytes(byte[] buf) {

        try (InputStream inputStream = new ByteArrayInputStream(buf)) {
            long version = Multihash.readVarint(inputStream);
            if (version != 1 && version != 0) {
                throw new Exception("invalid version");
            }
            long codec = Multihash.readVarint(inputStream);
            if (!(codec == Cid.DagProtobuf || codec == Cid.Raw || codec == Cid.Libp2pKey)) {
                throw new Exception("not supported codec");
            }

            int mhtype = (int) Multihash.readVarint(inputStream);

            long mhlen = Multihash.readVarint(inputStream);

            return new Prefix(codec, mhlen, mhtype, version);


        } catch (Throwable throwable) {
            throw new RuntimeException(throwable);
        }
    }

    @NonNull
    public static Cid sum(Prefix prefix, byte[] data) throws IOException {


        if (prefix.version == 0 && (!prefix.isSha2556()) ||
                (prefix.mhLength != 32 && prefix.mhLength != -1)) {
            throw new IOException("Invalid v0 Prefix");
        }

        if (!prefix.isSha2556()) {
            throw new IOException("Type Multihash " +
                    Multihash.Type.lookup(prefix.mhType).name() + " is not supported");
        }

        switch ((int) prefix.version) {
            case 0:
                return Cid.createCidV0(data);
            case 1:
                return Cid.createCidV1(prefix.codec, data);
            default:
                throw new IOException("Invalid cid version");
        }


    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Prefix prefix = (Prefix) o;
        return version == prefix.version && codec == prefix.codec && mhType == prefix.mhType && mhLength == prefix.mhLength;
    }

    @Override
    public int hashCode() {
        return Objects.hash(version, codec, mhType, mhLength);
    }

    public Multihash.Type getType() {
        return Multihash.Type.lookup(mhType);
    }

    public boolean isSha2556() {
        return Multihash.Type.lookup(mhType) == Multihash.Type.sha2_256;
    }

    @NonNull
    @Override
    public String toString() {
        return "Prefix{" +
                "version=" + version +
                ", codec=" + codec +
                ", mhType=" + Multihash.Type.lookup(mhType).name() +
                ", mhLength=" + mhLength +
                '}';
    }

    public byte[] bytes() {

        try (ByteArrayOutputStream out = new ByteArrayOutputStream()) {
            Multihash.putUvarint(out, version);
            Multihash.putUvarint(out, codec);
            Multihash.putUvarint(out, mhType);
            Multihash.putUvarint(out, mhLength);
            return out.toByteArray();
        } catch (Throwable throwable) {
            throw new RuntimeException(throwable);
        }
    }

    public long getVersion() {
        return version;
    }

    public long getCodec() {
        return codec;
    }
}
