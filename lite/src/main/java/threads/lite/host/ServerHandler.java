package threads.lite.host;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import net.luminis.quic.QuicConnection;
import net.luminis.quic.QuicStream;
import net.luminis.quic.server.ApplicationProtocolConnection;

import java.util.function.Consumer;

import threads.lite.cid.Multiaddr;

public class ServerHandler extends ApplicationProtocolConnection implements Consumer<QuicStream> {

    private static final String TAG = ServerHandler.class.getSimpleName();
    private final Session session;
    private final QuicConnection quicConnection;

    public ServerHandler(@NonNull Session session, @NonNull QuicConnection quicConnection) {
        this.session = session;
        this.quicConnection = quicConnection;
        quicConnection.setPeerInitiatedStreamCallback(this);
    }

    @Override
    public void accept(QuicStream quicStream) {
        new StreamHandler(session, quicConnection, quicStream) {
            @Nullable
            @Override
            public Multiaddr getObservedAddress() {
                return null;
            }
        };
    }
}
