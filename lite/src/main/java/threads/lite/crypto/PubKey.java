package threads.lite.crypto;


import androidx.annotation.NonNull;

import crypto.pb.Crypto;

public abstract class PubKey implements Key {

    private final Crypto.KeyType keyType;

    public PubKey(Crypto.KeyType keyType) {
        super();
        this.keyType = keyType;
    }

    public abstract boolean verify(byte[] data, byte[] signature);


    @NonNull
    public Crypto.KeyType getKeyType() {
        return this.keyType;
    }
}

