package threads.lite.host;


import net.luminis.quic.QuicStream;

import java.nio.ByteBuffer;

public interface StreamData extends TokenData {

    void data(QuicStream stream, ByteBuffer buffer) throws Exception;
}
