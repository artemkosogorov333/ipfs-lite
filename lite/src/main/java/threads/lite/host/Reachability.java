package threads.lite.host;

public enum Reachability {
    LOCAL, NONE, GLOBAL
}
