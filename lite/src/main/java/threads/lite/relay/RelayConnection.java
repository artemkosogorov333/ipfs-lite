package threads.lite.relay;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import net.luminis.quic.QuicConnection;
import net.luminis.quic.QuicConstants;
import net.luminis.quic.QuicStream;
import net.luminis.quic.RawStreamData;
import net.luminis.quic.Version;

import java.net.InetSocketAddress;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;

import threads.lite.cid.PeerId;

public class RelayConnection implements QuicConnection {
    private final PeerId self;
    private final PeerId peerId;
    private final QuicConnection conn;

    private RelayConnection(@NonNull QuicConnection conn,
                            @NonNull PeerId peerId,
                            @Nullable PeerId self) {
        this.conn = conn;
        this.peerId = peerId;
        this.self = self;
    }

    public static RelayConnection createRelayConnection(@NonNull QuicConnection conn,
                                                        @NonNull PeerId peerId) {
        return new RelayConnection(conn, peerId, null);
    }

    public static RelayConnection createRelayConnection(@NonNull QuicConnection conn,
                                                        @NonNull PeerId peerId,
                                                        @NonNull PeerId self) {
        return new RelayConnection(conn, peerId, self);
    }

    @Override
    public Version getQuicVersion() {
        return conn.getQuicVersion();
    }

    @Override
    public void setMaxAllowedBidirectionalStreams(int max) {
        conn.setMaxAllowedBidirectionalStreams(max);
    }

    @Override
    public void setMaxAllowedUnidirectionalStreams(int max) {
        conn.setMaxAllowedUnidirectionalStreams(max);
    }

    @Override
    public void setDefaultStreamReceiveBufferSize(long size) {
        conn.setDefaultStreamReceiveBufferSize(size);
    }

    @Override
    public synchronized CompletableFuture<QuicStream> createStream(
            @NonNull Consumer<RawStreamData> streamDataConsumer, boolean bidirectional,
            long timeout, TimeUnit timeUnit) {
        CompletableFuture<QuicStream> future = new CompletableFuture<>();
        try {
            QuicStream stream;
            if (self == null) {
                stream = RelayService.getStream(conn, peerId, timeout, timeUnit);
            } else {
                stream = RelayService.getStream(conn, peerId, self, timeout, timeUnit);
            }
            stream.setStreamDataConsumer(streamDataConsumer);
            future.complete(stream);
        } catch (Throwable throwable) {
            future.completeExceptionally(throwable);
        }
        return future;
    }

    @Override
    public void setPeerInitiatedStreamCallback(Consumer<QuicStream> streamConsumer) {
        throw new RuntimeException("not allowed");
    }

    @Override
    public void close() {
        conn.close();
    }

    @Override
    public void close(QuicConstants.TransportErrorCode applicationError, String errorReason) {
        conn.close(applicationError, errorReason);
    }

    @Override
    public boolean isConnected() {
        return conn.isConnected();
    }

    @Override
    public InetSocketAddress getRemoteAddress() {
        return conn.getRemoteAddress();
    }

    @NonNull
    @Override
    public String toString() {
        return "RelayConnection{" +
                "peerId=" + peerId +
                ", conn=" + conn +
                '}';
    }
}
