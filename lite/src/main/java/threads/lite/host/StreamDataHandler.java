package threads.lite.host;

import androidx.annotation.NonNull;

import net.luminis.quic.ImplementationError;
import net.luminis.quic.RawStreamData;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.util.function.Consumer;

import threads.lite.IPFS;
import threads.lite.LogUtils;
import threads.lite.cid.Multihash;

public class StreamDataHandler implements Consumer<RawStreamData> {
    private static final String TAG = StreamDataHandler.class.getSimpleName();
    @NonNull
    private final TokenData tokenData;
    private ByteBuffer buffer;
    private int expectedLength;
    private boolean reset;

    public StreamDataHandler(@NonNull StreamData streamData) {
        this.expectedLength = 0;
        this.tokenData = streamData;
    }

    public StreamDataHandler(@NonNull TokenData tokenData) {
        this.expectedLength = 0;
        this.tokenData = tokenData;
    }


    public void load(@NonNull byte[] data,
                     @NonNull Consumer<String> tokenConsumer,
                     @NonNull Consumer<ByteBuffer> dataConsumer) throws IOException {

        if (!reset) {
            if (expectedLength == 0) {
                try (InputStream inputStream = new ByteArrayInputStream(data)) {
                    expectedLength = (int) Multihash.readVarint(inputStream); // this is shit
                    if (expectedLength <= 0) {
                        throw new ImplementationError();
                    }
                    buffer = ByteBuffer.allocate(expectedLength);

                    int read = inputStream.read(buffer.array());
                    buffer.position(read);

                    if (read == expectedLength) {
                        evaluateBuffer(tokenConsumer, dataConsumer);
                        expectedLength = 0;
                    }

                    // check for a next iteration
                    if (inputStream.available() > 0) {
                        byte[] rest = new byte[inputStream.available()];
                        //noinspection ResultOfMethodCallIgnored
                        inputStream.read(rest);
                        load(rest, tokenConsumer, dataConsumer);
                    }
                }
            } else {
                buffer.put(data);
                if (buffer.position() == expectedLength) {
                    evaluateBuffer(tokenConsumer, dataConsumer);
                    expectedLength = 0;
                }
            }
        }
    }

    private void evaluateBuffer(@NonNull Consumer<String> tokenConsumer,
                                @NonNull Consumer<ByteBuffer> dataConsumer) {
        byte[] data = buffer.array();
        // expected to be for a token
        if (data[0] == '/' && data[data.length - 1] == '\n') {
            String token = new String(data, StandardCharsets.UTF_8);
            token = token.substring(0, data.length - 1);
            tokenConsumer.accept(token);
        } else if (data[0] == 'n' && data[1] == 'a' && data[data.length - 1] == '\n') {
            tokenConsumer.accept(IPFS.NA);
        } else if (data[0] == 'l' && data[1] == 's' && data[data.length - 1] == '\n') {
            tokenConsumer.accept(IPFS.LS);
        } else {
            dataConsumer.accept(buffer);
        }
    }

    private void reset() {
        try {
            buffer = null;
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        } finally {
            expectedLength = 0;
            reset = true;
        }
    }

    @Override
    public void accept(RawStreamData rawStreamData) {
        try {

            if (rawStreamData.isTerminated()) {
                reset();
            }

            load(rawStreamData.getStreamData(),
                    token -> {
                        try {
                            tokenData.token(rawStreamData.getStream(), token);
                        } catch (Throwable throwable) {
                            reset();
                            tokenData.throwable(throwable);
                        }
                    }, buffer -> {
                        try {
                            if (tokenData instanceof StreamData) {
                                StreamData streamData = (StreamData) tokenData;
                                streamData.data(rawStreamData.getStream(), buffer);
                            } else {
                                tokenData.throwable(
                                        new Exception("unexpected data arrives " + buffer.capacity()));
                            }
                        } catch (Throwable throwable) {
                            reset();
                            tokenData.throwable(throwable);
                        }
                    });

            if (rawStreamData.isFinal()) {
                tokenData.fin();
                reset();
            }

        } catch (Throwable exception) {
            LogUtils.error(TAG, "Expected length " + expectedLength);
            LogUtils.error(TAG, exception);
            tokenData.throwable(exception);
            reset();
        }
    }

}
