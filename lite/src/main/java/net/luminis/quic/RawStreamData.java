package net.luminis.quic;


public interface RawStreamData {

    QuicStream getStream();

    byte[] getStreamData();

    // if the stream data is final
    boolean isFinal();

    // if the stream is terminated by the other side
    boolean isTerminated();

}
